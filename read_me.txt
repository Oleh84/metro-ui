﻿Test task for JavaScript developer.

Technologies used: AJAX, SASS, Gulp.js

Description of the project folder:
metro-ui
  ├── build/         : RESULT FILES
  ├── node_modules/  : modules for GulpJs automation
  ├── src/           : source files
  ├── task/          : task files
  ├── Gulpfile.js    : act as a manifest to define the tasks that needs to execute
  └── package.json   : stores information about the project and dependencies used in it