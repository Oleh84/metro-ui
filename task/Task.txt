Test task for JavaScript developer

1. Please, mark up a layout � metro-ui.psd. Supported browsers: the latest version of Google Chrome.
Since it's not always possible to test the result on real mobile devices, it's enough for the layout to run
properly in emulation mode: https://developer.chrome.com/devtools/docs/mobile-emulation 
Bonus tasks: 
� Create pressed states for tiles. You may create an effect similar to Windows Phone 8, or do it the way
you like. 
� Responsive design. The layout must adapt to mobile devices running iOS and Android. Minimal
supported screen width � 320px. 

2. Add an interactive newsfeed (the data is loaded form the file news.json). Visually, it must look like
changing content on tiles of Windows 8 or later versions. The news are grouped by month. Within 1
group, news are shown randomly, change every 15 seconds, and do not reappear. After all news in all
groups are shown one by one, the algorithm returns to the news of the first group and starts the process
anew. Upon reloading the page, the algorithm starts the process anew.