/* Created by O.V.Horyn on May, 2016
 */

// ______________FUNCTIONS___________________


/* Begin
 * XMLHttpRequest functions - "http://www.quirksmode.org/js/xmlhttp.html"
 */
function sendRequest(url,callback,postData) {
    var req = createXMLHTTPObject();
    if (!req) return;
    var method = (postData) ? "POST" : "GET";
    req.open(method,url,true);
    req.setRequestHeader('User-Agent','XMLHTTP/1.0');
    if (postData)
        req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status != 200 && req.status != 304) {
//			alert('HTTP error ' + req.status);
            return;
        }
        callback(req);
    }
    if (req.readyState == 4) return;
    req.send(postData);
}

var XMLHttpFactories = [
    function () {return new XMLHttpRequest()},
    function () {return new ActiveXObject("Msxml2.XMLHTTP")},
    function () {return new ActiveXObject("Msxml3.XMLHTTP")},
    function () {return new ActiveXObject("Microsoft.XMLHTTP")}
];

function createXMLHTTPObject() {
    var xmlhttp = false;
    for (var i=0;i<XMLHttpFactories.length;i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}
/* End
 * XMLHttpRequest functions
 */

// Sort news in News Object randomly and grouped by month
function formatOrderInNewsArr( newsArr ){

    //Random sort function - "https://learn.javascript.ru/array-methods#%D1%81%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D1%8B%D0%B9-%D0%BF%D0%BE%D1%80%D1%8F%D0%B4%D0%BE%D0%BA-%D0%B2-%D0%BC%D0%B0%D1%81%D1%81%D0%B8%D0%B2%D0%B5"
    function compareRandom(a, b) {
        return Math.random() - 0.5;
    }

    // sort by Month function
    function compareMonth(newsA, newsB) {
        if (newsA.date.getYear() === newsB.date.getYear()) {
            return newsA.date.getMonth() - newsB.date.getMonth();
        }
        return newsA.date.getYear() - newsB.date.getYear();
    }

    newsArr.sort(compareRandom);
    newsArr.sort(compareMonth);

    return newsArr;
}

// Parse text to object
function parseToObj(text){
    return  JSON.parse(text, function(key, value) {
        // create a rule, the key date is always a date - "https://learn.javascript.ru/json#%D1%83%D0%BC%D0%BD%D1%8B%D0%B9-%D1%80%D0%B0%D0%B7%D0%B1%D0%BE%D1%80-json-parse-str-reviver"
        if (key == 'date') return new Date(value);
        return value; } );
}

// Output arrey whith objects in HTML element
function OutputInNewsfeedBlock(Arr){

    var newsArr = Arr;

    var period = 15000;
    var time= -period/2;

    // Date output options
    var dateOutputOptions = {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };

    setInterval(function () {

        newsArr = formatOrderInNewsArr(newsArr);
        document.getElementById('news_title').innerHTML = "Blog";

        newsArr.forEach(function(news, i, newsArr) {

            setTimeout(function () {
                document.getElementById('news_header').innerHTML = news.header;
                document.getElementById('news_date').innerHTML = news.date.toLocaleString("en-US", dateOutputOptions);
                document.getElementById('news_block').style.animation = "frame_up 1s ease-out forwards";
            }, time += period/2);

            setTimeout(function () {
                document.getElementById('news_body').innerHTML = news.body;
                document.getElementById('news_block').style.animation = "frame_down 1s ease-out forwards ";
            }, time += period/2);

        });

    }, 500);

}

// handleRequest for XMLHttpRequest functions
function handleRequest(req) {

    //Message about data loading
    document.getElementById('news_title').innerHTML = "Loading...";

    newsObj = parseToObj( req.responseText );
    var newsArr = newsObj.news;

    OutputInNewsfeedBlock(newsArr);

}


// ______________MAIN_PART___________________


var newsObj;

//The message is keeping when it's impossible to get data
document.getElementById('news_title').innerHTML = "no data..";

// Use XMLHttpRequest functions
sendRequest("./data/news.json",handleRequest);

/* Alternative way to load data from file.
 * Chrome does not allow XMLHttpRequest for local files (file:/// * / *.html).
 * This way allows to load data from file in Chrome without server when XMLHttpRequest is blocked by browser.
 */
setTimeout(function () { // waiting for XMLHttpRequest realization

    if (newsObj == undefined){

        newsObj = parseToObj(altNewsStr);

        //Message about data loading from alternative source
        document.getElementById('news_title').innerHTML = "Alternative loading...";

        var newsArr = newsObj.news;

        OutputInNewsfeedBlock(newsArr);
    }

}, 1000);